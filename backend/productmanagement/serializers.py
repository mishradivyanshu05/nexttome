from rest_framework import serializers
from .models import Product, ProductCategory, ProductSubCategory, Unit
from rest_framework.exceptions import ValidationError


class ProductSubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductSubCategory
        fields = '__all__'

    def validate_subcategory_name(self, subcategory_name):
        if ProductSubCategory.objects.filter(category__id=self.initial_data.get('category'),
                                             subcategory_name__iexact=subcategory_name).exists():
            raise ValidationError("SubCategory Name Already Exists with this category")
        return subcategory_name


class ProductCategorySerializer(serializers.ModelSerializer):
    subcategory = ProductSubCategorySerializer(many=True, read_only=True)

    class Meta:
        model = ProductCategory
        fields = '__all__'
        extra_fields = ['subcategory']

    def validate_category_name(self, category_name):
        if ProductCategory.objects.filter(shop__user=self.context.get('request').user,
                                          category_name__iexact=category_name).exists():
            raise ValidationError("Category Name Already Exists")
        return category_name


class ProductSerializer(serializers.ModelSerializer):
    unitName = serializers.ReadOnlyField(source='unit.name', read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
        extra_kwargs = {
            'shop': {'required': False}, 'other_detail': {'required': False}, 'sac_hsn': {'required': False}}


class UnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unit
        fields='__all__'
        extra_kwargs = {'shop': {'required': False}}
