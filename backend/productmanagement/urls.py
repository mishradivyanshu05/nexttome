from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'unit/$', views.UnitClass.as_view(), name='unit-operation'),
    url(r'product-category/$', views.ProductCategoryList.as_view(), name='product-category'),
    url(r'product-subcategory/$', views.CreateProductSubCategory.as_view(), name='product-subcategory'),
    url(r'add-product/$', views.AddProduct.as_view(), name='add-product'),
    url(r'product/(?P<pk>[0-9]+)/$', views.ProductOperation.as_view(), name='product')
]
