from common.models import CreateModifyModel
from django.db import models
from django.contrib.postgres.fields import ArrayField

from vendor.models import ShopDetail


class Unit(CreateModifyModel):
    name = models.CharField(max_length=250)
    code = models.CharField(max_length=250)
    shop = models.ForeignKey(ShopDetail, on_delete=models.CASCADE, null=True, blank=True, default=None)


class ProductCategory(CreateModifyModel):
    category_name = models.CharField(max_length=50)
    shop = models.ForeignKey(ShopDetail,on_delete=models.CASCADE)

    def __str__(self):
        return self.category_name


class ProductSubCategory(CreateModifyModel):
    subcategory_name = models.CharField(max_length=250)
    category = models.ForeignKey(ProductCategory,on_delete=models.CASCADE, related_name="subcategory")

    def __str__(self):
        return self.subcategory_name


class Product(CreateModifyModel):
    product_image_urls = ArrayField(models.CharField(max_length=300), blank=True, null=True)
    product_name = models.CharField(max_length=300)
    other_detail = models.JSONField(null=True)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE, null=True, blank=True)
    subcategory = models.ForeignKey(ProductSubCategory, on_delete=models.CASCADE,null=True,blank=True)
    short_description = models.TextField(null=True, default='')
    shop = models.ForeignKey(ShopDetail,on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=20, decimal_places=2, default=0.00, null=True)
    in_stock = models.BooleanField(default=True)
