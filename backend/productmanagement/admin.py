from django.contrib import admin
from .models import Product, Unit, ProductCategory, ProductSubCategory

admin.site.register(Product)
admin.site.register(ProductCategory)
admin.site.register(Unit)
admin.site.register(ProductSubCategory)
