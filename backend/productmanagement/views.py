from django.db.models import Q
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response

from productmanagement.models import Unit, Product, ProductCategory
from productmanagement.serializers import UnitSerializer, ProductSerializer, ProductSubCategorySerializer, \
    ProductCategorySerializer
from vendor.models import ShopDetail


class UnitClass(APIView):

    def get(self, request):
        unit = Unit.objects.filter(Q(shop__user=self.request.user) | Q(shop=None)).order_by('-id')
        serializer = UnitSerializer(unit, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UnitSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(shop=ShopDetail.objects.get(user=self.request.user))
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    serializer_class = ProductSubCategorySerializer


class AddProduct(generics.ListCreateAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        product_name = self.request.query_params.get('name')
        queryset = Product.objects.filter(shop=ShopDetail.objects.get(user=self.request.user)).order_by('-id')
        if product_name:
            queryset = queryset.filter(name__icontains=product_name)
        return queryset

    def perform_create(self, serializer):
        pro_obj=serializer.save(shop=ShopDetail.objects.get(user=self.request.user))


class ProductOperation(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ProductSerializer
    lookup_url_kwarg = 'pk'

    def get_serializer(self, *args, **kwargs):
        kwargs['partial'] = True
        return super(ProductOperation, self).get_serializer(*args, **kwargs)

    def get_queryset(self):
        return Product.objects.filter(shop__user=self.request.user)

    def perform_update(self, serializer):
        pro_obj=serializer.save()


class ProductCategoryList(generics.ListCreateAPIView):

    """Return the list of categories with subcategories"""

    serializer_class = ProductCategorySerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        data['shop'] = ShopDetail.objects.get(user=self.request.user).id
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        return ProductCategory.objects.filter(shop__user=self.request.user).prefetch_related('subcategory')


class CreateProductSubCategory(generics.CreateAPIView):
    """Return the list of categories."""

    serializer_class = ProductSubCategorySerializer
