from django.urls import path
from django.conf.urls import url
from rest_framework_simplejwt import views as jwt_views
from .views import Register , Login, OTPVerify, ProfileCreateAPIView, ProfileUpdateDestroyAPIView, GetFeeds

urlpatterns = [
    path('token/',
         jwt_views.TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('token/refresh/',
         jwt_views.TokenRefreshView.as_view(),
         name='token_refresh'),
    url(r'register/$', Register.as_view(), name='register-nex2-me'),
    url(r'login/$', Login.as_view(), name='register-nex2-me'),
    url(r'otp_verify/$', OTPVerify.as_view(), name='register-nex2-me'),
    url(r'profile/$', ProfileCreateAPIView.as_view(), name='register-nex2-me'),
    path('get_profile_details/<int:id>/', ProfileUpdateDestroyAPIView.as_view(), name='userdetail'),
    url(r'get_feeds/$', GetFeeds.as_view(), name='userdetail'),
]
