from django_filters import rest_framework as filters

from vendor.models import ShopDetail


class ShopFilter(filters.FilterSet):
    sub_category = filters.MultipleChoiceFilter()

    class Meta:
        model = ShopDetail
        fields = ('sub_category', )
