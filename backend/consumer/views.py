import datetime
import json

from django.contrib.auth import authenticate
from django.shortcuts import render
from rest_framework import status, generics
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from vendor.models import MobileVerify, ShopDetail, ShopLocation, Category, SubCategory, ShopAlbum
from consumer.models import Profile, Address
from vendor.serializers import ShopDetailSerializer, ShopAlbumSerializer
from .filters import ShopFilter
from .serializers import ProfileSerializer, AddressSerializer, ProfileUpdateSerializer
from vendor.utils import send_sms, random_with_N_digits, get_tokens_for_user
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from common.mixins import SmallPagesPagination, MultiUpdateMixin


class GetFeeds(generics.ListAPIView):
    serializer_class = ShopAlbumSerializer
    # filter_class = ShopFilter
    pagination_class = SmallPagesPagination
    queryset = ShopAlbum.objects.all()

    def get_queryset(self):
        interests = self.request.query_params.getlist('sub_category')
        shop_list = ShopDetail.objects.filter(sub_category__id__in=interests).values_list('id')
        return ShopAlbum.objects.filter(shop__id__in=shop_list).select_related('shop')


class ProfileCreateAPIView(generics.CreateAPIView):
    """
    Profile Create API
    """
    serializer_class = ProfileSerializer


class ProfileUpdateDestroyAPIView(MultiUpdateMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    Profile Update API
    """

    serializer_class = ProfileUpdateSerializer
    queryset = Profile.objects.all()
    lookup_field = "id"
    related_field_name = "address_details"
    related_model = Address
    related_serializer_class = AddressSerializer
    instance_field = "profile"


class OTPVerify(APIView):

    permission_classes = [AllowAny]

    def post(self, request):
        mobile = request.data.get('mobile')
        otp = request.data.get('otp')
        user_send = MobileVerify.objects.filter(number=mobile).exists()
        if user_send:
            user_send_detail = MobileVerify.objects.get(number=mobile)
            user_send_time = user_send_detail.otp_time
            # now = datetime.datetime.now()-datetime.timedelta(hours=5,minutes=30)
            now = datetime.datetime.now()
            # print(now,user_send_time)
            if now.year == user_send_time.year and now.month == user_send_time.month and now.day == user_send_time.day:
                diff = now.hour - user_send_time.hour
                if diff > 1:
                    return Response({"message": "Otp entered has expired"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    if user_send_detail.otp == int(otp):
                        user_send_detail.status = True
                        user_send_detail.save()
                        user = User.objects.get(username=mobile)
                        user.is_active = True
                        user.save()
                        return Response({"message": "User created"}, status=status.HTTP_201_CREATED)
                    else:
                        return Response({"message": "Otp WRONG"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "Otp entered has expired"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "First Register Mobile No."}, status=status.HTTP_400_BAD_REQUEST)


class Register(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        password1 = request.data.get("password")
        password2 = request.data.get("confirm_password")
        tnc = request.data.get("is_accept")
        mobile = request.data.get("mobile")
        if tnc and password1 and password2 and password2 == password1:
            if mobile:
                user_exists = User.objects.filter(username=mobile).exists()
                if user_exists:
                    user = User.objects.get(username=mobile)
                    if user.is_active:
                        return Response({"message": "User with this mobile number already exists.", "flag": False},
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        user.set_password(password1)
                else:
                    user = User.objects.create_user(username=mobile, password=password1)
                    user.is_active = False
                    user.save()
                if not MobileVerify.objects.filter(number=mobile).exists():
                    otp = random_with_N_digits(6)
                    res = send_sms(mobile, otp)
                    mobile_obj = MobileVerify.objects.create(number=mobile, otp=otp, otp_time=datetime.datetime.now(),
                                                             status=0)
                    mobile_obj.save()
                else:
                    otp = random_with_N_digits(6)
                    res = send_sms(mobile, otp)
                    mobile_obj = MobileVerify.objects.get(number=mobile)
                    mobile_obj.otp_time = datetime.datetime.now()
                    mobile_obj.otp = otp
                    mobile_obj.save()
                return Response({"message": "Otp sent", "details": json.loads(res), "is_otp": True},
                                status=status.HTTP_201_CREATED)
        else:
            return Response({"message": "Please check the form filled."}, status=status.HTTP_400_BAD_REQUEST)
        return Response({"message": "Please fill either Mobile or Email"}, status=status.HTTP_201_CREATED)


class Login(APIView):

    permission_classes = [AllowAny]

    def post(self, request):
        name = request.data.get("username")
        password = request.data.get("password")
        try:
            user_exists = User.objects.filter(username=name)
            if not user_exists.exists():
                return Response({"message": "User with this details not exists.", "flag": False},
                                status=status.HTTP_400_BAD_REQUEST)
            user_obj = authenticate(username=user_exists[0].username, password=password)
            if user_obj:
                if user_obj.is_active:
                    user_token = get_tokens_for_user(user_obj)
                    profile_exists = Profile.objects.filter(user=user_obj)
                    return Response({"message": "User Logged in",
                                     "token": user_token["access"],
                                     "username": user_obj.username,
                                     "user_id": user_obj.id,
                                     "flag": profile_exists.exists(),
                                     "profile_id":profile_exists[0].id if len(profile_exists)!=0 else '' }, status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Please activate your mobile number to login.", "flag": False},
                                    status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({"message": 'Password Incorrect', "flag": False}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response(
                {'message': 'Please enter a valid username and password.', "details": str(e), "flag": False},
                status=status.HTTP_401_UNAUTHORIZED)


