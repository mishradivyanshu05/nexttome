from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models
import uuid

from common.models import CreateModifyModel
from vendor.models import SubCategory


class Profile(CreateModifyModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    first_name = models.CharField(max_length=250)
    #interests = models.ManyToManyField(SubCategory, related_name='profile', null=True, blank=True)
    interests = ArrayField(models.CharField(max_length=15), null=True, blank=True)
    following = ArrayField(models.CharField(max_length=15), null=True, blank=True)
    dir_name = models.CharField(null=True, max_length=250, blank=True)
    contact = ArrayField(models.CharField(max_length=15), null=True, blank=True)
    profilepictureurl = models.URLField(max_length=999, null=True, blank=True)
    bannerpictureurl = models.URLField(max_length=999, null=True, blank=True)


class Address(CreateModifyModel):
    pincode = models.CharField(max_length=10)
    state = models.CharField(max_length=50)
    state_code = models.CharField(max_length=50)
    street_details = models.CharField(max_length=50)
    landmark = models.CharField(null=True, max_length=50)
    city = models.CharField(null=True, max_length=50)
    profile = models.ForeignKey(Profile, related_name="address_details",
                                on_delete=models.CASCADE)
    uuid = models.UUIDField(
        unique=True, default=uuid.uuid4, editable=False,
        db_index=True
    )


