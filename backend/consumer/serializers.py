from django.db import transaction

import json

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from consumer.models import Profile, Address


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = "__all__"
        extra_kwargs = {'profile': {'required': False}}
        read_only_fields = ("profile",)


class ProfileSerializer(serializers.ModelSerializer):
    address_details = AddressSerializer(many=True)

    @transaction.atomic

    def create(self, validated_data):
        address_details_data = validated_data.pop("address_details")
        profile = Profile.objects.create(**validated_data)
        for address_data in address_details_data:
            Address.objects.create(
                profile=profile, **address_data)
        return profile

    def update(self, instance, validated_data):
        print(validated_data)
        address_details_data = validated_data.pop("address_details")
        print(address_details_data)
        for address_data in address_details_data:
            print(address_data)
            address_obj = Address.objects.filter(id=address_data["id"]
                                                 ).update(**address_details_data)
        profile = Profile.objects.filter(id=validated_data['id']).update(**validated_data)

    class Meta:
        model = Profile
        fields = "__all__"


class ProfileUpdateSerializer(serializers.ModelSerializer):

    address_details = AddressSerializer(many=True, read_only=True)

    class Meta:
        model = Profile
        fields = "__all__"
