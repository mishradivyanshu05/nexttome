from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework.response import Response


class SmallPagesPagination(PageNumberPagination):
    page_size = 5


class MultiUpdateMixin(APIView):

    def update(self, request, *args, **kwargs):

        if self.related_field_name != "":
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            existing_related_instances = getattr(instance, self.related_field_name).values_list("uuid", flat=True)
            related_instances_data = request.data.get(self.related_field_name)
        else:
            class_instance = self.get_instance()
            instance = self.instance_class.objects.get(id=self.request.data[0].get(self.instance_field))
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            existing_related_instances = class_instance.values_list("uuid", flat=True)
            related_instances_data = request.data
        self.perform_update_multiple(existing_related_instances, related_instances_data, instance)
        return Response(serializer.data)

    def perform_update_multiple(self, existing_obj, data, instance):

        update_obj_data = []
        create_obj_data = []
        print('inupdate', data)

        for obj in data:
            if obj.get("uuid"):
                update_obj_data.append(obj)
            else:
                create_obj_data.append(obj)

        update_obj = [
            obj["uuid"] for obj
            in update_obj_data
        ]

        delete_obj = [
            str(obj_uuid) for obj_uuid in
            existing_obj if str(obj_uuid) not in update_obj
        ]

        opts = self.related_model._meta.concrete_model._meta
        foreign_keys =[]
        for field in [field for field in opts.fields if
                      field.serialize and field.remote_field]:
            field_name = field
            related_model = field.remote_field.model
            foreign_key = ((str(field_name)).split(".")[-1], related_model)
            foreign_keys.append(foreign_key)

        # Update Objects
        for obj_data in update_obj_data:
            obj_serializer = self.related_serializer_class(data=obj_data)
            obj_serializer.is_valid(raise_exception=True)
            obj_instance = self.related_model.objects.get(uuid=obj_data.pop("uuid"))
            serializer_data = obj_serializer.data
            for keys in foreign_keys:
                foreign_instance_field = keys[0]
                foreign_class = keys[1]
                if foreign_instance_field in serializer_data.keys():
                    foreign_instance = foreign_class.objects.get(id=serializer_data[foreign_instance_field])
                    setattr(obj_instance, foreign_instance_field, foreign_instance)
                    serializer_data.pop(foreign_instance_field)
            for attr, value in serializer_data.items():
                setattr(obj_instance, attr, value)
            obj_instance.save()

        # Create Objects
        for obj_data in create_obj_data:
            obj_serializer = self.related_serializer_class(data=obj_data)
            obj_serializer.is_valid(raise_exception=True)
            serializer_data = obj_serializer.data
            query = {}
            for keys in foreign_keys:
                foreign_instance_field = keys[0]
                foreign_class = keys[1]
                if foreign_instance_field in serializer_data.keys():
                    foreign_instance = foreign_class.objects.get(id=serializer_data[foreign_instance_field])
                    query[foreign_instance_field] = foreign_instance
                    serializer_data.pop(foreign_instance_field)
            query[self.instance_field] = instance
            self.related_model.objects.create(**query, **serializer_data)