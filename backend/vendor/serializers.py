import json

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from vendor.image_upload import add_object_to_bucket
from vendor.models import ShopDetail, ShopLocation, Category, SubCategory, Address, ShopAlbum


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'


class ShopDetailSerializer(serializers.ModelSerializer):
    address = AddressSerializer(read_only=True)

    lat_lon = serializers.SerializerMethodField()

    def validate(self, data):
        add_serializer = AddressSerializer(data = self.initial_data)
        if add_serializer.is_valid():
            data['address']  = add_serializer.save()
            return data
        else:
            raise ValidationError(add_serializer.errors)

    class Meta:
        model = ShopDetail
        fields = '__all__'
        extra_fields = ['lat_lon', 'category']
        extra_kwargs = {'user': {'required': False}, 'address': {'required': False}}


    def get_lat_lon(self, instance):
        location_obj = ShopLocation.objects.filter(shop__user=instance.user)
        if (location_obj):
            return json.dumps({"lon": location_obj[0].lon, "lat": location_obj[0].lat})
        else:
            return None


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = "__all__"


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = "__all__"


class ShopAlbumSerializer(serializers.ModelSerializer):
    shop_name = serializers.ReadOnlyField(source='shop.buisness_name', read_only=True)
    shop_picture = serializers.ReadOnlyField(source='shop.profilepictureurl', read_only=True)

    def create(self, validated_data):
        validated_data['shop'] = self.context['request'].user.shopdetails
        shop_album = ShopAlbum.objects.create(**validated_data)
        return validated_data

    class Meta:
        model = ShopAlbum
        fields = "__all__"
        extra_fields = ['shop_name','shop_picture']
        extra_kwargs = {
            'shop': {'required': False}}


class ShopDetailUpdateSerializer(serializers.ModelSerializer):
    address = AddressSerializer(read_only=True)

    lat_lon = serializers.SerializerMethodField()

    class Meta:
        model = ShopDetail
        fields = '__all__'
        extra_fields = ['lat_lon', 'category']
        extra_kwargs = {'user': {'required': False}, 'address': {'required': False}}

    def get_lat_lon(self, instance):
        location_obj = ShopLocation.objects.filter(shop__user=instance.user)
        if (location_obj):
            return json.dumps({"lon": location_obj[0].lon, "lat": location_obj[0].lat})
        else:
            return None
