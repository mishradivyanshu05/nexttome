from django.urls import path
from django.conf.urls import url
from rest_framework_simplejwt import views as jwt_views
from .views import Register , Login, OTPVerify, ShopRegister, CategoryListView,\
    SubCategoryListView, GetShopDetail, ShopAlbumCreateApiView, ShopAlbumDestroyView, \
    GetAllSubCategory, GetShopDetailById, ShopAlbumListApiViewByShopId

urlpatterns = [
    path('token/',
         jwt_views.TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('token/refresh/',
         jwt_views.TokenRefreshView.as_view(),
         name='token_refresh'),
    url(r'register/$', Register.as_view(), name='register-nex2-me'),
    url(r'login/$', Login.as_view(), name='register-nex2-me'),
    url(r'otp_verify/$', OTPVerify.as_view(), name='register-nex2-me'),
    url(r'shop_registeration/$', ShopRegister.as_view(), name='register-nex2-me'),
    url(r'list_categories/$', CategoryListView.as_view(), name='register-nex2-me'),
    path(r'list_sub_categories/<int:category_id>/', SubCategoryListView.as_view(), name='register-nex2-me'),
    path(r'list_all_sub_categories/', GetAllSubCategory.as_view(), name='register-nex2-me'),
    url(r'get_details/$', GetShopDetail.as_view(), name='userdetail'),
    path(r'get_detail_by_id/<int:id>/', GetShopDetailById.as_view(), name='shopdetailget'),
    path(r'shop_album_delete/<int:id>/', ShopAlbumDestroyView.as_view(), name='shop_album_detail_delete'),
    path(r'shop_album_by_shop_id/<int:id>/', ShopAlbumListApiViewByShopId.as_view(), name='shop_album_get_by_id'),
    url(r'shop_album/$', ShopAlbumCreateApiView.as_view(), name='shop_album_details'),
]
