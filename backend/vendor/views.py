import datetime
import json

from django.contrib.auth import authenticate
from django.shortcuts import render
from rest_framework import status, generics
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from vendor.models import MobileVerify, ShopDetail, ShopLocation, Category, SubCategory, ShopAlbum
from .serializers import ShopDetailSerializer, ShopDetailUpdateSerializer, CategorySerializer, SubCategorySerializer, ShopAlbumSerializer, AddressSerializer
from .utils import send_sms, random_with_N_digits, get_tokens_for_user
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

class OTPVerify(APIView):

    permission_classes = [AllowAny]

    def post(self, request):
        mobile = request.data.get('mobile')
        otp = request.data.get('otp')
        user_send = MobileVerify.objects.filter(number=mobile).exists()
        if user_send:
            user_send_detail = MobileVerify.objects.get(number=mobile)
            user_send_time = user_send_detail.otp_time
            # now = datetime.datetime.now()-datetime.timedelta(hours=5,minutes=30)
            now = datetime.datetime.now()
            # print(now,user_send_time)
            if now.year == user_send_time.year and now.month == user_send_time.month and now.day == user_send_time.day:
                diff = now.hour - user_send_time.hour
                if diff > 1:
                    return Response({"message": "Otp entered has expired"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    if user_send_detail.otp == int(otp):
                        user_send_detail.status = True
                        user_send_detail.save()
                        user = User.objects.get(username=mobile)
                        user.is_active = True
                        user.save()
                        return Response({"message": "User created"}, status=status.HTTP_201_CREATED)
                    else:
                        return Response({"message": "Otp WRONG"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "Otp entered has expired"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "First Register Mobile No."}, status=status.HTTP_400_BAD_REQUEST)

class Register(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        password1 = request.data.get("password")
        password2 = request.data.get("confirm_password")
        tnc = request.data.get("is_accept")
        mobile = request.data.get("mobile")
        if tnc and password1 and password2 and password2 == password1:
            if mobile:
                user_exists = User.objects.filter(username=mobile).exists()
                if user_exists:
                    user = User.objects.get(username=mobile)
                    if user.is_active:
                        return Response({"message": "User with this mobile number already exists.", "flag": False},
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        user.set_password(password1)
                else:
                    user = User.objects.create_user(username=mobile, password=password1)
                    user.is_active = False
                    user.save()
                if not MobileVerify.objects.filter(number=mobile).exists():
                    otp = random_with_N_digits(6)
                    res = send_sms(mobile, otp)
                    mobile_obj = MobileVerify.objects.create(number=mobile, otp=otp, otp_time=datetime.datetime.now(),
                                                             status=0)
                    mobile_obj.save()
                else:
                    otp = random_with_N_digits(6)
                    res = send_sms(mobile, otp)
                    mobile_obj = MobileVerify.objects.get(number=mobile)
                    mobile_obj.otp_time = datetime.datetime.now()
                    mobile_obj.otp = otp
                    mobile_obj.save()
                return Response({"message": "Otp sent", "details": json.loads(res), "is_otp": True},
                                status=status.HTTP_201_CREATED)
        else:
            return Response({"message": "Please check the form filled."}, status=status.HTTP_400_BAD_REQUEST)
        return Response({"message": "Please fill either Mobile or Email"}, status=status.HTTP_201_CREATED)


class ShopRegister(APIView):

    def post(self, request, *args, **kwargs):
        try:
            serializer = ShopDetailSerializer(data=request.data, context = {'request': self.request})
            if serializer.is_valid():
                shop_obj = serializer.save(user=self.request.user)
                #ShopLocation.objects.create(shop=shop_obj, lat=request.data.get('latitude'), lon=request.data.get('longitude'))
                return Response({"message":"Shop created","flag":True},status=status.HTTP_201_CREATED)
            else:
                print(serializer.errors)
                return Response({"message": serializer.errors, "flag": False, "latitude": "Please provide this if not present", "longitude": "Please provde this is not present."}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'message': 'User Not Found', "flag": False, "error": str(e)},
                            status=status.HTTP_400_BAD_REQUEST)


class GetShopDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ShopDetailUpdateSerializer
    queryset = ShopDetail.objects.all()
    lookup_field = "id"

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, user=self.request.user)
        return obj

    def put(self, request):
        shop_obj = self.get_object()
        address_obj = shop_obj.address
        address_serializer = AddressSerializer(address_obj, data=self.request.data)
        shop_serialier = ShopDetailUpdateSerializer(shop_obj, data=self.request.data)
        if address_serializer.is_valid():
            if shop_serialier.is_valid():
                address_serializer.save()
                shop_serialier.save()
                return Response({"message": "Details Updates", 'data': {**shop_serialier.data, **address_serializer.data}})
            else:
                return Response({"message": "Shop Details is not valid", "error": shop_serialier.errors}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Address Details is not valid", "error": address_serializer.errors},
                            status=status.HTTP_400_BAD_REQUEST)


class GetShopDetailById(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ShopDetailUpdateSerializer
    queryset = ShopDetail.objects.all()
    lookup_field = "id"


class Login(APIView):

    permission_classes = [AllowAny]

    def post(self, request):
        name = request.data.get("username")
        password = request.data.get("password")
        try:
            user_exists = User.objects.filter(username=name)
            if not user_exists.exists():
                return Response({"message": "User with this details not exists.", "flag": False},
                                status=status.HTTP_400_BAD_REQUEST)
            user_obj = authenticate(username=user_exists[0].username, password=password)
            if user_obj:
                if user_obj.is_active:
                    user_token = get_tokens_for_user(user_obj)
                    shop_exists = ShopDetail.objects.filter(user=user_obj).exists()
                    return Response({"message": "User Logged in", "token":user_token["access"], "username":user_obj.username,"flag":shop_exists},status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Please activate your mobile number to login.", "flag": False},
                                    status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({"message": 'Password Incorrect', "flag": False}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response(
                {'message': 'Please enter a valid username and password.', "details": str(e), "flag": False},
                status=status.HTTP_401_UNAUTHORIZED)


class CategoryListView(generics.ListAPIView):

    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class SubCategoryListView(generics.ListAPIView):

    serializer_class = SubCategorySerializer

    def get_queryset(self):
        return SubCategory.objects.filter(category=self.kwargs.get('category_id'))


class GetAllSubCategory(generics.ListAPIView):
    serializer_class = SubCategorySerializer
    queryset = SubCategory.objects.all()


class ShopAlbumDestroyView(generics.DestroyAPIView):

    serializer_class = ShopAlbumSerializer
    lookup_field = "id"
    queryset = ShopAlbum.objects.all()


class ShopAlbumCreateApiView(generics.ListCreateAPIView):

    serializer_class = ShopAlbumSerializer

    def get_queryset(self):
        return ShopAlbum.objects.filter(shop__user=self.request.user)


class ShopAlbumListApiViewByShopId(generics.ListAPIView):

    serializer_class = ShopAlbumSerializer

    def get_queryset(self):
        return ShopAlbum.objects.filter(shop__id=self.kwargs.get('id'))