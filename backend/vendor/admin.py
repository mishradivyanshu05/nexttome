from django.contrib import admin
from .models import Category, SubCategory, ShopDetail, Address

admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(ShopDetail)
admin.site.register(Address)