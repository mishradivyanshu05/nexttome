from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

from common.models import CreateModifyModel


class Category(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)


class Address(CreateModifyModel):
    pincode = models.CharField(max_length=10)
    state = models.CharField(max_length=50)
    state_code = models.CharField(max_length=50)
    street_details = models.CharField(max_length=50)
    landmark = models.CharField(null=True, max_length=50)
    city = models.CharField(null=True, max_length=50)


class ShopDetail(CreateModifyModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='shopdetails')
    buisness_name = models.CharField(max_length=250)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    sub_category =  models.ManyToManyField(SubCategory, related_name='shopdetails', null=True, blank=True)
    dir_name = models.CharField(null=True, max_length=250, blank=True)
    gst_number = models.CharField(null=True, max_length=500, blank=True)
    contact = ArrayField(models.CharField(max_length=15), null=True, blank=True)
    profilepictureurl = models.URLField(max_length=999, null=True, blank=True)
    bannerpictureurl = models.URLField(max_length=999, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    online_buisness = models.BooleanField(default=False)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)


class ShopLocation(models.Model):
    shop = models.OneToOneField(ShopDetail,on_delete=models.CASCADE)
    lon = models.CharField(max_length=20, null=True, blank=True)
    lat = models.CharField(max_length=20, null=True, blank=True)


class MobileVerify(CreateModifyModel):
    otp = models.IntegerField(default=0)
    number = models.CharField(null=True, max_length=20)
    otp_time = models.DateTimeField()
    status = models.BooleanField(default=False)


class ShopAlbum(CreateModifyModel):
    shop = models.ForeignKey(ShopDetail, on_delete=models.CASCADE)
    album_urls = ArrayField(models.URLField(max_length=500), null=True, blank=True)
    caption = models.TextField(null=True, blank=True)

    # def save(self, *args, **kwargs):
    #     self.shop = ShopDetail.objects.get()
    #     super().save(*args, **kwargs)
