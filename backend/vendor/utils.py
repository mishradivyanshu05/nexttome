from random import randint

import requests
from rest_framework_simplejwt.tokens import RefreshToken


def send_sms(mobile,otp):
    #url = "http://108.175.9.81/app/smsapi/index.php?key=25C67CCA20382E&campaign=0&routeid=50&type=text&contacts=%s&senderid=SENDER&msg=Your+Otp+is+%s"%(mobile,otp)
    url = "https://2factor.in/API/V1/2bddc4a5-61a1-11eb-8153-0200cd936042/SMS/%s/%s/TEST"%(mobile,otp)
    r = requests.get(url)
    return r.content.decode('utf-8')

def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }
